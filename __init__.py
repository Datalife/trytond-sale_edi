# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from . import configuration
from . import sale


def register():
    Pool.register(
        configuration.SaleConfiguration,
        sale.Sale,
        sale.SaleLine,
        module='sale_edi', type_='model')
    Pool.register(
        sale.ImportSales,
        module='sale_edi', type_='wizard')

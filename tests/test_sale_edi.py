# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
import unittest
import os
import shutil
import trytond.tests.test_tryton
from trytond.tests.test_tryton import ModuleTestCase, with_transaction
from trytond.pool import Pool
from trytond.modules.company.tests import create_company, set_company
from trytond.modules.account.tests import create_chart, get_fiscalyear
from trytond.modules.account_invoice.tests import set_invoice_sequences
from decimal import Decimal
import datetime


TEST_FILES_DIR = os.path.join(os.path.dirname(__file__), 'data', 'tmp')
TEST_FILES_EXTENSION = '.txt'


def create_fiscalyear_and_chart(company=None, fiscalyear=None,
        chart=True):
    'Test fiscalyear'
    pool = Pool()
    FiscalYear = pool.get('account.fiscalyear')
    if not company:
        company = create_company()
    with set_company(company):
        if chart:
            create_chart(company)
        if not fiscalyear:
            fiscalyear = set_invoice_sequences(get_fiscalyear(company))
            fiscalyear.save()
            FiscalYear.create_period([fiscalyear])
        return fiscalyear


def get_accounts(company):
    pool = Pool()
    Account = pool.get('account.account')
    accounts = {}
    accounts['receivable'] = Account.search([
        ('type.name', '=', 'Receivable'),
        ('company', '=', company.id),
        ])[0]
    accounts['payable'] = Account.search([
        ('type.name', '=', 'Payable'),
        ('company', '=', company.id),
        ])[0]

    accounts['revenue'] = Account.search([
        ('type.name', '=', 'Revenue'),
        ('company', '=', company.id),
        ])[0]
    accounts['expense'] = Account.search([
        ('type.name', '=', 'Expense'),
        ('company', '=', company.id),
        ])[0]
    return accounts


def create_parties(company):
    pool = Pool()
    Party = pool.get('party.party')
    with set_company(company):
        return Party.create([{
                'name': 'customer1',
                'addresses': [('create', [{
                    'edi_ean': 'PUNTO_VENTA'}])]
            },
            {
                'name': 'supplier1',
                'addresses': [('create', [{
                    'active': False}])],
                'active': False
            }])


def create_payment_term():
    PaymentTerm = Pool().get('account.invoice.payment_term')
    term, = PaymentTerm.create([{
        'name': '0 days',
        'lines': [
            ('create', [{
                'sequence': 0,
                'type': 'remainder',
                'relativedeltas': [('create', [{}])],
            }])]
        }])
    return term


def get_currency():
    pool = Pool()
    Currency = pool.get('currency.currency')
    CurrencyRate = pool.get('currency.currency.rate')

    currency = Currency(
        name='Euro',
        symbol='€',
        code='EUR',
        rounding=Decimal('0.01'))
    currency.save()

    CurrencyRate(
            date=datetime.date.min,
            rate=Decimal('2.0'),
            currency=currency).save()
    return currency


class TestCase(ModuleTestCase):
    """Test Sale Edi module"""
    module = 'sale_edi'

    def create_products_cross_reference(self, accounts, customer, supplier):
        pool = Pool()
        CrossReference = pool.get('product.cross_reference')
        ProductCategory = pool.get('product.category')
        Template = pool.get('product.template')
        Uom = pool.get('product.uom')
        Product = pool.get('product.product')

        expense = accounts.get('expense')
        unit, = Uom.search([('name', '=', 'Unit')], limit=1)

        category = ProductCategory(name='test account used',
                account_expense=expense, accounting=True)
        category.save()

        template, = Template.create([{
            'name': 'Product 1',
            'type': 'goods',
            'default_uom': unit.id,
            'account_category': category,
            'salable': True,
            'sale_uom': unit.id,
            'active': True,
            'list_price': Decimal(10),
            'cost_price_method': 'fixed',
            'categories': [['add', [category.id]]]}])
        product = Product()
        product.template = template
        product.code = '67310'
        product.ean_code = '8437014044118'
        product.save()

        cross_reference = CrossReference()
        cross_reference.party = customer
        cross_reference.product = product
        cross_reference.name = 'Product Ref 1'
        cross_reference.ean_code = '8437014044118'
        cross_reference.save()
        return product

    @with_transaction()
    def test_get_sales_from_edi_file(self):
        pool = Pool()
        SaleConfig = pool.get('sale.configuration')
        Location = pool.get('stock.location')
        Tax = pool.get('account.tax')
        Sale = pool.get('sale.sale')

        if not os.path.exists(TEST_FILES_DIR):
            os.mkdir(TEST_FILES_DIR)
        test_fname = (os.path.join(os.path.dirname(__file__), 'data',
            'order%s' % TEST_FILES_EXTENSION))
        shutil.copy(test_fname, TEST_FILES_DIR)
        currency = get_currency()

        warehouse, = Location.search([('code', '=', 'WH')])
        company = create_company(currency=currency)
        with set_company(company):
            create_fiscalyear_and_chart(company, None, True)

            # Create some parties
            customer, supplier = create_parties(company)
            accounts = get_accounts(company)
            rate = Decimal('.10')
            tax = Tax()
            tax.name = 'Tax %s' % rate
            tax.description = tax.name
            tax.type = 'percentage'
            tax.rate = rate
            tax.invoice_account = accounts.get('tax')
            tax.credit_note_account = accounts.get('tax')
            term = create_payment_term()

            customer.edi_operational_point = 'PUNTO_VENTA'
            customer.customer_payment_term = term
            customer.save()
            sale_cfg = SaleConfig(1)
            sale_cfg.edi_source_path = os.path.abspath(TEST_FILES_DIR)
            sale_cfg.save()

            result_product = self.create_products_cross_reference(
                accounts, customer, supplier)
            sales = Sale.get_sales_from_edi_files()
            self.assertTrue(sales)
            sale = sales[0]
            self.assertEqual(sale.payment_term, term)
            self.assertEqual(sale.shipment_party, customer)
            self.assertEqual(sale.party, customer)
            self.assertTrue(sale.lines)
            line = sale.lines[0]
            self.assertEqual(line.product.code, result_product.code)
            shutil.rmtree(TEST_FILES_DIR)


def suite():
    suite = trytond.tests.test_tryton.suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(TestCase))
    return suite

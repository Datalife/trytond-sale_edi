#!/usr/bin/env python
# encoding: utf-8

from setuptools import setup, find_packages
import re
import os
import io
import configparser

MODULE2PREFIX = {
    'party_edi': 'nantic',
    'edocument_edifact': 'datalife'
}


def read(fname):
    return io.open(
        os.path.join(os.path.dirname(__file__), fname),
        'r', encoding='utf-8').read()


def get_require_version(name):
    if minor_version % 2:
        require = '%s >= %s.%s.dev0, < %s.%s'
    else:
        require = '%s >= %s.%s, < %s.%s'
    require %= (name, major_version, minor_version,
        major_version, minor_version + 1)
    return require

config = configparser.ConfigParser()
config.readfp(open('tryton.cfg'))
info = dict(config.items('tryton'))
for key in ('depends', 'extras_depend', 'xml'):
    if key in info:
        info[key] = info[key].strip().splitlines()

version = info.get('version', '0.0.1')
major_version, minor_version, _ = version.split('.', 2)
major_version = int(major_version)
minor_version = int(minor_version)
name = 'nantic_sale_edi'
download_url = 'https://github.com/nan-tic/trytond-sale_edi'

series = '%s.%s' % (major_version, minor_version)
if minor_version % 2:
    branch = 'default'
else:
    branch = series

dependency_links = {
    'party_edi':
        'git+https://gitlab.com/datalifeit/'
        'trytond-party_edi@%(branch)s'
        '#egg=nantic_party_edi' % {
            'branch': branch,
            },
    'edocument_edifact':
        'git+https://gitlab.com/datalifeit/'
        'trytond-edocument_edifact@%(branch)s'
        '#egg=datalifeit_edocument_edifact' % {
            'branch': branch,
        }
}

requires = [
    'oyaml',
    'edifact @ git+http://github.com/nan-tic/python-edifact'
]
for dep in info.get('depends', []):
    if not re.match(r'(ir|res)(\W|$)', dep):
        prefix = MODULE2PREFIX.get(dep, 'trytond')
        req = get_require_version('%s_%s' % (prefix, dep))
        if dep in dependency_links:
            req = '%s_%s @ %s' % (prefix, dep, dependency_links[dep])
        requires.append(req)
requires.append(get_require_version('trytond'))

tests_require = [get_require_version('proteus')]

setup(name=name,
    version=version,
    description='',
    long_description=read('README'),
    author='NaN·tic',
    author_email='info@nan-tic.com',
    url='http://www.nan-tic.com/',
    download_url=download_url,
    keywords='',
    package_dir={'trytond.modules.sale_edi': '.'},
    packages=(
        ['trytond.modules.sale_edi'] +
        ['trytond.modules.sale_edi.%s' % p for p in find_packages()]
    ),
    package_data={
        'trytond.modules.sale_edi': (info.get('xml', [])
            + ['tryton.cfg', 'locale/*.po', 'tests/*.rst', 'templates/*.yml',
                'tests/data/*']),
        },
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: Plugins',
        'Framework :: Tryton',
        'Intended Audience :: Developers',
        'Intended Audience :: Financial and Insurance Industry',
        'Intended Audience :: Legal Industry',
        'License :: OSI Approved :: GNU General Public License (GPL)',
        'Natural Language :: Bulgarian',
        'Natural Language :: Catalan',
        'Natural Language :: Czech',
        'Natural Language :: Dutch',
        'Natural Language :: English',
        'Natural Language :: French',
        'Natural Language :: German',
        'Natural Language :: Russian',
        'Natural Language :: Spanish',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Topic :: Office/Business',
        ],
    license='GPL-3',
    install_requires=requires,
    zip_safe=False,
    entry_points="""
    [trytond.modules]
    sale_edi = trytond.modules.sale_edi
    """,
    test_suite='tests',
    test_loader='trytond.test_loader:Loader',
    tests_require=tests_require,
    )
